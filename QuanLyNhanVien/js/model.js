function NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam){
    this.taiKhoan = taiKhoan;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tongLuong = function(){
        if(this.chucVu == "Sếp"){
            return this.luongCoBan * 3;
        }
        else if(this.chucVu == "Trưởng phòng"){
            return this.luongCoBan * 2;
        }
        else{
            return this.luongCoBan;
        }
    }
    this.xepLoai = function(){
        if(this.gioLam >= 192){
            return "Nhân viên xuất sắc";
        }
        else if(this.gioLam >= 176){
            return "Nhân viên giỏi";
        }
        else if(this.gioLam >= 160){
            return "Nhân viên khá";
        }
        else{
            return "Nhân viên trung bình";
        }
    }
}