function layThongTinTuForm(){
    var taiKhoan = document.querySelector("#tknv").value;
    var hoTen = document.querySelector("#name").value;
    var email = document.querySelector("#email").value;
    var matKhau = document.querySelector("#password").value;
    var ngayLam = document.querySelector("#datepicker").value;
    var luongCoBan = document.querySelector("#luongCB").value*1;
    var chucVu = document.querySelector("#chucvu").value;
    var gioLam = document.querySelector("#gioLam").value*1;
    return new NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam);
}

function renderNhanVien(dsnv){
    var contentHTML = "";
    for(var i=0; i<dsnv.length; i++){
        var nhanVien = dsnv[i];
        var content = `
            <tr>
                <td>${nhanVien.taiKhoan}</td>
                <td>${nhanVien.hoTen}</td>
                <td>${nhanVien.email}</td>
                <td>${nhanVien.ngayLam}</td>
                <td>${nhanVien.chucVu}</td>
                <td>${nhanVien.tongLuong()}</td>
                <td>${nhanVien.xepLoai()}</td>
                <td>
                    <button onclick = "suaNhanVien('${nhanVien.taiKhoan}')" class = "btn btn-warning" data-toggle="modal"
                    data-target="#myModal">Sửa</button>
                    <button onclick = "xoaNhanVien('${nhanVien.taiKhoan}')" class = "btn btn-danger">Xóa</button>
                </td>
            </tr>
        `;
        contentHTML += content;
    }

    document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

function saveIntoLocalStorage(dsnv){
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV",dataJson);
}

function getDataFromLocalStorage(){
    var dataJson = localStorage.getItem("DSNV");
    if(dataJson != null){
        var dsnv = JSON.parse(dataJson);
        dsnv = dsnv.map(function(nhanVien){
            return new NhanVien(nhanVien.taiKhoan, nhanVien.hoTen, nhanVien.email, nhanVien.matKhau, nhanVien.ngayLam, nhanVien.luongCoBan, nhanVien.chucVu, nhanVien.gioLam);
        })
        return dsnv;
    }
    return [];
}

function showThongTinLenForm(nhanVien){
    document.querySelector("#tknv").value = nhanVien.taiKhoan;
    document.querySelector("#name").value = nhanVien.hoTen;
    document.querySelector("#email").value = nhanVien.email;
    document.querySelector("#password").value = nhanVien.matKhau;
    document.querySelector("#datepicker").value = nhanVien.ngayLam;
    document.querySelector("#luongCB").value = nhanVien.luongCoBan;
    document.querySelector("#chucvu").value = nhanVien.chucVu;
    document.querySelector("#gioLam").value = nhanVien.gioLam;
}