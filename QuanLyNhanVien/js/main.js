// Tạo mảng để lưu danh sách sinh viên
var dsnv = [];
// Lấy dữ liệu từ localstorage rồi show lên web
dsnv = getDataFromLocalStorage();
renderNhanVien(dsnv);

function themNguoiDung(){
    // Lấy thông tin từ form và tạp object để luu
    var nhanVien = layThongTinTuForm();
    // Validate 
    var isValid = validateAll(nhanVien,dsnv);
    if(isValid){
        //Thêm nhân viên đã thỏa vào danh sách
        dsnv.push(nhanVien);
        // render nhân viên lên table   
        renderNhanVien(dsnv);
        // reset form
        document.querySelector("#formQLNV").reset();
    }
    // Lưu dsnv dưới dạng Json vào localstorage
    saveIntoLocalStorage(dsnv);
}

function xoaNhanVien(taiKhoan){
    var index = dsnv.findIndex(function(nhanVien){
        return nhanVien.taiKhoan == taiKhoan;
    })
    dsnv.splice(index,1);
    renderNhanVien(dsnv);
    saveIntoLocalStorage(dsnv);
}

function suaNhanVien(taiKhoan){
    var index = dsnv.findIndex(function(nhanVien){
        return nhanVien.taiKhoan == taiKhoan;
    });
    // Hiển thị thôn gtin lên form
    showThongTinLenForm(dsnv[index]);
    document.getElementById("tknv").disabled = true;
}

function updateNhanVien(){
    var nhanVien = layThongTinTuForm();
    var isValid = validateAllUpdate(nhanVien);
    if(isValid){
        var index = dsnv.findIndex(function(nv){
            return nhanVien.taiKhoan == nv.taiKhoan;
        })
        // Update nhân viên
        dsnv[index] = layThongTinTuForm();
        renderNhanVien(dsnv);
        document.querySelector("#formQLNV").reset();
    }
    saveIntoLocalStorage(dsnv);
}

function showNhanVienTheoLoai(){
    var loaiNhanVien = document.getElementById("searchName").value;
    var dsnv_new = [];
    for(var i=0; i<dsnv.length; i++){
        if(dsnv[i].xepLoai() == loaiNhanVien){
            dsnv_new.push(dsnv[i]);
        }
    }
    renderNhanVien(dsnv_new);
}

