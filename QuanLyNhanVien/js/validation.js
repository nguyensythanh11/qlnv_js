function showMessage(spanId, message){
    document.getElementById(spanId).style.display = "block";
    document.getElementById(spanId).innerHTML = message;
}

function checkTaiKhoanTrung(spanId, message, dsnv, nhanVien){
    var index = dsnv.findIndex(function(nv){
        return nv.taiKhoan == nhanVien.taiKhoan;
    })
    if(index == -1){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId,message);
    return false;
}

function checkTaiKhoan(min, max, spanId, message, nhanVien){
    var length = nhanVien.taiKhoan.length;
    if(length == 0){
        showMessage(spanId,"Bạn vui lòng nhập tài khoản");
        return false;
    }
    if(length >= min && length <= max){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId, message);
    return false;
}

function checkTenNhanVien(spanId, message, nhanVien){
    const re = /^[a-zA-Z ]*$/;
    if(nhanVien.hoTen.length == 0){
        showMessage(spanId,"Bạn vui lòng nhập họ và tên");
        return false;
    }
    if(re.test(nhanVien.hoTen)){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId, message);
    return false;
}

function checkEmail(spanId, message, nhanVien){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(nhanVien.email.length == 0){
        showMessage(spanId,"Bạn vui lòng nhập email")
        return false;
    }
    if(re.test(nhanVien.email)){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId, message);
    return false;
}

function checkPassword(spanId, message, nhanVien){
    if(nhanVien.matKhau.length == 0){
        showMessage(spanId,"Bạn vui lòng nhập mật khẩu")
        return false;
    }
    const re = /^(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/
    if(re.test(nhanVien.matKhau)){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId, message);
    return false;
}

function checkNgayLam(spanId, message, nhanVien){
    if(NhanVien.ngayLam == ""){
        showMessage(spanId,"Bạn vui lòng nhập ngày làm");
        return false;
    }
    var date_regex = /^\d{2}\/\d{2}\/\d{4}$/;
    if(date_regex.test(nhanVien.ngayLam)){
        showMessage(spanId, "");
        return true;
    }
    showMessage(spanId, message);
    return false;
}

function checkLuongCoBan(spanId, message, nhanVien){
    if(nhanVien.luongCoBan == ""){
        showMessage(spanId,"Bạn vui lòng nhập lương cơ bản");
        return false;
    }
    if(nhanVien.luongCoBan >= 1e6 && nhanVien.luongCoBan <= 20*1e6){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId,message);
    return false;
}

function checkChucVu(spanId, message, nhanVien){
    if(nhanVien.chucVu != "Chọn chức vụ"){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId,message);
    return false;
}

function checkGioLam(spanId, message, nhanVien){
    if(nhanVien.gioLam == ""){
        showMessage(spanId,"Bạn vui lòng nhập số giờ làm");
        return false;
    }
    if(nhanVien.gioLam >= 80 && nhanVien.gioLam <= 200){
        showMessage(spanId,"");
        return true;
    }
    showMessage(spanId,message);
    return false;
}

function validateAll(nhanVien, dsnv){
    // Validate cho tài khoản
    var isValid = checkTaiKhoan(4,6,"tbTKNV","Tài khoản có độ dài từ 4 đến 6 ký số",nhanVien) &&
    checkTaiKhoanTrung("tbTKNV","Tài khoản đã tồn tại",dsnv,nhanVien)
    ;
    // Validate cho tên nhân viên
    isValid = isValid & checkTenNhanVien("tbTen","Họ và tên chỉ bao gồm chữ cái",nhanVien);
    // Validate cho email
    isValid = isValid & checkEmail("tbEmail","Email không đúng định dạng",nhanVien);
    // Validate cho mật khẩu
    isValid = isValid & checkPassword("tbMatKhau","Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)",nhanVien);
    // Validate cho ngày làm
    isValid = isValid & checkNgayLam("tbNgay","Bạn vui lòng nhập theo định dạng mm/dd/yyyy",nhanVien);
    // Validate cho lương cơ bản
    isValid = isValid & checkLuongCoBan("tbLuongCB","Lương cơ bản từ 1 000 000 đến 20 000 000",nhanVien);
    // Validate cho chức vụ
    isValid = isValid & checkChucVu("tbChucVu","Bạn vui lòng chọn chức vụ", nhanVien);
    // Validate cho giờ làm
    isValid = isValid & checkGioLam("tbGiolam","Số giờ làm trong tháng 80 - 200 giờ",nhanVien);

    return isValid;
}

function validateAllUpdate(nhanVien){
    // Validate cho tài khoản
    var isValid = checkTaiKhoan(4,6,"tbTKNV","Tài khoản có độ dài từ 4 đến 6 ký số",nhanVien);
    // Validate cho tên nhân viên
    isValid = isValid & checkTenNhanVien("tbTen","Họ và tên chỉ bao gồm chữ cái",nhanVien);
    // Validate cho email
    isValid = isValid & checkEmail("tbEmail","Email không đúng định dạng",nhanVien);
    // Validate cho mật khẩu
    isValid = isValid & checkPassword("tbMatKhau","Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)",nhanVien);
    // Validate cho ngày làm
    isValid = isValid & checkNgayLam("tbNgay","Bạn vui lòng nhập theo định dạng mm/dd/yyyy",nhanVien);
    // Validate cho lương cơ bản
    isValid = isValid & checkLuongCoBan("tbLuongCB","Lương cơ bản từ 1 000 000 đến 20 000 000",nhanVien);
    // Validate cho chức vụ
    isValid = isValid & checkChucVu("tbChucVu","Bạn vui lòng chọn chức vụ", nhanVien);
    // Validate cho giờ làm
    isValid = isValid & checkGioLam("tbGiolam","Số giờ làm trong tháng 80 - 200 giờ",nhanVien);

    return isValid;
}